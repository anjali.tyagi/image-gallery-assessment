# Image Gallery
## Description

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.9. It is a simple image gallery built using Angular 17 that incorporates an infinite scroll effect. It displays a list of images in a grid layout and dynamically loads more images as the user scrolls down the page. It allows users to view each image in a larger size by clicking on it.
This project utilizes Angular component based on MVC architecture, services and routing to create a seamless user experience.

## Features

- Display a grid layout of images
- Click on an image to view it in larger size with details
- Infinite Scrolling using Intersection Observer API
- Angular routing for navigation between pages

## Installation
1. clone the repository: `git clone https://gitlab.com/anjali.tyagi/image-gallery-assessment.git`
2. Install dependencies: `npm install`
3. Run the project: `ng serve`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Usage

1. Open the project in your browser
2. Browse through the images in the gallery.
3. Scroll down the page to dynamically load more images.
4. Click on an image to view it in a larger size with details

## Technologies Used

- Angular 17
- HTML5
- Sass

## Credits
This project was created by Anjali Tyagi as part of a assessment in Angular development.

Feel free to customize this template with specific details about your project. If you have any further questions or need assistance, please don't hesitate to ask.