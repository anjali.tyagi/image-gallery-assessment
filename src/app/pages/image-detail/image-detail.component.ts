import { Location } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IImage } from '../../core/models/image';
import { ImageGalleryService } from '../../core/service/images/image-gallery.service';

@Component({
  selector: 'app-image-detail',
  standalone: true,
  imports: [],
  templateUrl: './image-detail.component.html',
  styleUrl: './image-detail.component.scss'
})
export class ImageDetailComponent {
  ImageId: string='';
  imageDetail: undefined | IImage
  
  imageService = inject(ImageGalleryService);
  private route = inject(ActivatedRoute);
  private location = inject(Location);

  ngOnInit(): void {
    let id = Number(this.route.snapshot.paramMap.get('imageId'))
    this.getImageDetails(id);
  }

  getImageDetails(id: number) {
    this.imageService.getImageDetail(id).subscribe((res)=>{
      this.imageDetail = res;
    });
  }

  goBack(): void {
    this.location.back();
  }
}
