import { Component, ElementRef, inject, QueryList, ViewChildren } from '@angular/core';
import { RouterLink } from '@angular/router';
import { IImage } from '../../core/models/image';
import { ImageGalleryService } from '../../core/service/images/image-gallery.service';

@Component({
  selector: 'app-image-list',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './image-list.component.html',
  styleUrl: './image-list.component.scss'
})
export class ImageListComponent {
  @ViewChildren('lastElement', { read: ElementRef })
  lastElement!: QueryList<ElementRef>;

  imageList: IImage [] = [];
  currentPage: number = 1;
  observer: any;

  imageService = inject(ImageGalleryService);

  ngOnInit(): void {
    this.getAllImage();
    this.intersectionObserver();
  }

  ngAfterViewInit(){
    this.lastElement.changes.subscribe((m) => {
      if(m.last) this.observer.observe(m.last.nativeElement)
    })
  }

  getAllImage() {
    this.imageService.getAllImage(this.currentPage).subscribe({
      next: (res) => {
        this.imageList = [...this.imageList, ...res];
      },
      error: (error) =>{
      console.log(`error from API => ${error}`)
      }
    })
  }

  intersectionObserver() {
    let options = {
      root: null,
      rootMargin: '0px',
      threshold:0.25
    };

    this.observer = new IntersectionObserver((entries) => {
      if(entries[0].isIntersecting){
        this.currentPage++;
        this.getAllImage();
      }
    }, options);
  }
}
