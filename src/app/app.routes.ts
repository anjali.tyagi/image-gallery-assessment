import { Routes } from '@angular/router';
import { ImageDetailComponent } from './pages/image-detail/image-detail.component';
import { ImageListComponent } from './pages/image-list/image-list.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: ImageListComponent
    },
    {
        path: 'detail',
        component: ImageDetailComponent
    },
    {
        path: 'detail/:imageId',
        component: ImageDetailComponent
    },
    {
        path: '**',
        component: ImageListComponent
    }
];
