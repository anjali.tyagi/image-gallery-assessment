import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { APIConstant } from '../../constant/APIConstant';
import { IImage } from '../../models/image';
import { MainService } from '../main/main.service';

@Injectable({
  providedIn: 'root'
})
export class ImageGalleryService {

  constructor(private main: MainService) { }

  getAllImage(page: number): Observable<IImage[]> {
    return this.main.get(`${APIConstant.author.getAllAuthors}?page=${page}&limit=30`)
  }

  getImageDetail(authorId: number): Observable<IImage> {
    return this.main.get(`id/${authorId}/info`)
  }
}
